extends KinematicBody2D


export var speed = 100
enum State {IDLE, MOVE, AIR}
var state = IDLE

var input_direction = Vector2()

func _ready():
	self.set_process_input(true)
	self.set_physics_process(true)
	#$AnimationPlayer.play("Idle")

func _input(event):
	if event.is_action_pressed("move_left"):
		self.st_machine(State.MOVE)
		$Pivot.scale = Vector2(-1, 1)
		self.input_direction = Vector2(-1, 0)
	elif event.is_action_pressed("move_right"):
		self.st_machine(State.MOVE)
		$Pivot.scale = Vector2(1, 1)
		self.input_direction = Vector2(1, 0)
	elif event.is_action_pressed("move_down"):
		self.st_machine(State.MOVE)
		$Pivot.scale = Vector2(1, 1)
		self.input_direction = Vector2(0, 1)
	elif event.is_action_pressed("move_up"):
		self.st_machine(State.MOVE)
		$Pivot.scale = Vector2(1, 1)
		self.input_direction = Vector2(0, -1)
	elif (not Input.is_action_pressed("move_left") and
	      not Input.is_action_pressed("move_right") and
	      not Input.is_action_pressed("move_down") and
	      not Input.is_action_pressed("move_up")):
		self.st_machine(State.IDLE)

func st_machine(new_state):
	if new_state != self.state:
		match [self.state, new_state]:
			[IDLE, MOVE]:
				print("idle -> move")
			[MOVE, IDLE]:
				print("move -> idle")
			_:
				print("Illegal transition")
		self.state = new_state

func _physics_process(delta):
	if self.state == State.MOVE:
		var motion = input_direction.normalized() * self.speed * delta
		self.move_and_collide(motion)
